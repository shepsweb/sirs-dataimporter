== Installation ==
composer require sirs/dataimporter
vendor publish and select dataimporter
php artisan migrate

== Configuration ==
In data_importer.php, set up your fileToRowProcessors.  This allows you to specify a directory where imports of a given type will be stored, and associate them with a specific class to be used to process the import file's rows.

== How To Use ==
1.  In your row processor class, use Sirs\DataImporter\Interfaces\DataImporterModel and make sure your class implements DataImporterModel. (requires that your row processor implement getCSVKey() and getCSVColToPropertyMap(), but see notes as I think it also requires at least processRow()).  If you don't want your row processor to also be the model, then your row processor should also extend the model (for using $this->fillable() in csvToAtributes() mentioned in notes)
2. On the model related to what you will be importing, use Sirs\DataImporter\Traits\DataImporterTraits and use DataImporterTraits.  This sets up a belongs to relationship with the data_imports table and that model.  Need to make sure you have a "data_import_id" field on that model.
3. To use relative paths, make sure that you:
    A. Add a 'useRelativePaths' key to the data_importer config ('useRelativePaths' => true, ...)
    B.  In the filesToRowProcessors key of the data importer config, you use as the keys paths relative to the base path of your application
    C.  When running the command, make sure to specify a relative path as the path argumnent and add the --relative-paths flag (e.g., php artisan data:import storage/app/CCNC_Physician_Network , --relative-paths)


== Still to document ==
needsViewCreated()?
pretty much need csvToAttributes() function in row processor (add to interface?  or assume it so it doesn't have to be copied over and over)
need processRow() function in your processor(add to interface?)
When an import fails, the record is still created in data_imports so either you have to delete that record of fix anything in the csv and save it as a new file.
When I save a .xlsx as .csv, a weird character is added as the first in the file.  So, I have to open up text edit, delete the first letter on the first line, then re-type it.
distincttion between import model interface and row processor interface, the latter of which does require processRow()
CsvProcessor Requires that app.primary_programmer.email and app.primary_programmer.name are configured for sending e-mails.
