<?php

namespace Sirs\DataImporter\Exceptions;

use Exception;


class DataImportException extends Exception{
	/**
	 * @var null
	 */
	private $data_import_id;


	/**
	 * @param string $aMessage
	 * @param null $data_import_id
	 * @param int $code
	 * @param Exception|null $previous
	 */
	public function __construct( $aMessage, &$data_import_id = NULL, $code = 0, Exception $previous = NULL)
	{
		// make sure everything is assigned properly
		parent::__construct($aMessage, $code, $previous);
		$this->data_import_id = $data_import_id;
	}

	/**
	 * @return null
	 */
	public function getDataImportId()
	{
		return $this->data_import_id;
	}

	/**
	 * @param null $data_import_id
	 */
	public function setDataImportId($data_import_id)
	{
		$this->data_import_id = $data_import_id;
	}
}