<?php

namespace Sirs\DataImporter\Exceptions;

use Exception;


class DataImportInvalidRowException extends Exception{
	/**
	 * @var null
	 */
	private $errors;


	/**
	 * @param string $aMessage
	 * @param array $errors
	 * @param int $code
	 * @param Exception|null $previous
	 */
	public function __construct( $aMessage, $errors = null, $code = 0, Exception $previous = NULL)
	{
		// make sure everything is assigned properly
		parent::__construct($aMessage, $code, $previous);
		$this->errors = $errors;
	}

	/**
	 * @return null
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * @param null $data_import_id
	 */
	public function setErrors($errors)
	{
		$this->errors = $errors;
	}
}