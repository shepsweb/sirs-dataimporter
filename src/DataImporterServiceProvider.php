<?php 
namespace Sirs\DataImporter;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Sirs\DataImporter\Console\Commands\ImportData;

class DataImporterServiceProvider extends ServiceProvider {

  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot()
  {
    $this->publishes([ __DIR__ . '/../config/config.php' => config_path('data_importer.php') ], 'config');
    $this->publishes([ __DIR__ . '/database/migrations/' => database_path('/migrations') ], 'migrations');

    $this->loadViewsFrom(__DIR__.'/Resources/Views', 'dataimporter');

    $this->commands([
        ImportData::class,
    ]);
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {

  }


}
