<?php

namespace Sirs\DataImporter;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Mail;
use Sirs\DataImporter\CsvProcessor;
use Sirs\DataImporter\DataImportStatus;
use Sirs\DataImporter\Exceptions\DataImportException;
use Sirs\DataImporter\Interfaces\DataImporterModel;

class DataImport extends Model
{
    protected $fillable = [
        "id",
        "data_import_status_id",
        "file_path",
        "json_errors",
        "created_at",
        "updated_at",
    ];

    /**
     * Array of exceptions caught during this upload
     * @var null
     */
    public $errors = [];

    /**
     * Array of CSVData extracted from file
     * @var null
     */
    public $csvData = null;

    /**
     * String used to more easily identify rows in a CSV file when a composite key is used
     */
    public $csvCompositeKey = null;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo*
     */

    public function dataImportStatus()
    {
        return $this->belongsTo(DataImportStatus::class);
    }

    /**
     * Description: Split CSV data into arrays to be written to relevant models
     * @param DataImporterModel $rowProcessorClass
     * @param $folderPath
     * @param $fileName
     * @param $delimiter
     * @return bool
     */
    public function process($delimiter){
        $fileProcessor = new CsvProcessor();
        $fileProcessor->setDelimiter($delimiter);
        $fileProcessor->processFile($this);
    }
}
