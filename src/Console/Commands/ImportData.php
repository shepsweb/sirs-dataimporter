<?php

namespace Sirs\DataImporter\Console\Commands;

use Illuminate\Console\Command;
use Sirs\DataImporter\DataImport;
use Sirs\DataImporter\Exceptions\DataImportException;

class ImportData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:import
                               {folder_path : import measures from a specified path ex. storage/vidant}
                               {delimiter : file delimeter for parsing}
                               {--relative-paths : whether you are using relative paths or not, defaults to using absolute paths if this flag isn\'t specified}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrapes folder path and imports files and attempts to save them as their respective model defined in config folder_models array';

    /**
     * Execute the command.
     *
     * @throws DataImportException
     */
    public function handle()
    {
        ini_set("memory_limit", "256M");
        ini_set('max_execution_time', '0');
        ini_set('max_input_time', '0');
        ini_set('max_allowed_packet', '0');
        ini_set('auto_detect_line_endings', true);
        set_time_limit(0);
        ignore_user_abort(true);

        $folderPathArg = $this->argument('folder_path');
        $delimiter = $this->argument('delimiter');
        $useRelativePaths = $this->option('relative-paths');
        $relativePath = $useRelativePaths ? $folderPathArg : null;
        $folderPath = $useRelativePaths ? base_path($folderPathArg) : $folderPathArg;

        $modelNames = config('data_importer.filesToRowProcessors');
        // TODO: would be nice to accomodate paths with and without trailing slashes

        // lookup folder path
        if ($useRelativePaths) {
            if (!array_key_exists($relativePath, $modelNames)) {
                throw new DataImportException('Unable to find the relevant model name for path: '.$relativePath);
            }
        } else {
            if (!array_key_exists($folderPath, $modelNames)) {
                throw new DataImportException('Unable to find the relevant model name for path: '.$folderPath);
            }
        }

        // make sure path ends in a trailing slash (because that's what is expected)
        if (substr($folderPathArg, strlen($folderPathArg)-1) != '/') {
            $folderPathArg = $folderPathArg . '/';
        }
        if (substr($folderPath, strlen($folderPath)-1) != '/') {
            $folderPath = $folderPath . '/';
        }
        if (substr($relativePath, strlen($relativePath)-1) != '/') {
            $relativePath = $relativePath . '/';
        }

        // Loop through all files in the folder path and process them
        $folder_path_all_csv = $folderPath . '*.[TtCc][XxSs][TtVv]'; // glob needs the * on the end to get all files
        foreach (glob($folder_path_all_csv) as $file) {
            if (!is_dir($file)) { // we don't care about sub-directories

                // check to see if an import attempt has already been made for this file
                if ($useRelativePaths) {
                    if (DataImport::where('file_path', $relativePath . basename($file))->first()) {
                        continue;
                    }
                } else {
                    if (DataImport::where('file_path', $folderPath . basename($file))->first()) {
                        continue;
                    }
                }

                // Try to process the file
                if ($useRelativePaths) {
                    $dataImport = DataImport::create([
                        'file_path' => $relativePath . basename($file),
                        'data_import_status_id'=>1
                    ]);
                } else {
                    $dataImport = DataImport::create([
                        'file_path' => $folderPath . basename($file),
                        'data_import_status_id'=>1
                    ]);
                }

                $lineCount = explode(' ', trim(shell_exec('wc -l "'.$dataImport->file_path.'"')));

                $this->info('processing '.$dataImport->file_path.' - '.$lineCount[0].' lines...');

                $dataImport->process($delimiter);
            }
        }
    }
}
