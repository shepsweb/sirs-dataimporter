<?php
namespace Sirs\DataImporter\Interfaces;

use Sirs\DataImporter\DataImport;

/**
 * Interface for file processors.
 *
 * @package default
 * @author 
 **/
interface FileProcessor
{
	public function __construct();

	public function processFile(DataImport $dataImport);
} // END interface FileProcessor