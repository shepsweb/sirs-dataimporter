<?php 
namespace Sirs\DataImporter\Interfaces;

use Sirs\DataImporter\DataImport;

interface RowProcessor
{

    /*
   * returns the column in the expected CSV file that represents a unique key to be referenced for errors messages and the like
   */
  public function getCSVKey();

  /*
   * returns an array that maps csv column names to model properties if they are different (only include those columns that are different, otherwise importer will use the property name)
   */
  public function getCSVColToPropertyMap();

  
  /**
   * Processes the row based on implementation
   * @return [type] [description]
   * @throws \Exception Thrown when row has invalid data or has some other problem
   */
  public function processRow($csvRow, DataImport $dataImport);

}
?>