<?php namespace Sirs\DataImporter\Interfaces;

interface DataImporterModel
{

    /*
   * returns the column in the expected CSV file that represents a unique key to be referenced for errors messages and the like
   */
  public static function getCSVKey();

  /*
   * returns an array that maps csv column names to model properties if they are different (only include those columns that are different, otherwise importer will use the property name)
   */
  public function getCSVColToPropertyMap();

}
?>