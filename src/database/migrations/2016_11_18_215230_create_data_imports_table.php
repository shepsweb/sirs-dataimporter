<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_imports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('data_import_status_id')->unsigned();
            $table->string('file_path')->nullable();
            $table->json('json_errors')->nullable();
            $table->timestamps();

            $table->foreign('data_import_status_id')->references('id')->on('data_import_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('data_imports');
    }
}
