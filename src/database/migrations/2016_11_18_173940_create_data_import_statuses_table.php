<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sirs\DataImporter\DataImportStatus;

class CreateDataImportStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_import_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Eloquent::unguard();

        DataImportStatus::create([
            "id" => 1,
            "name" => "Pending"
        ]);
        DataImportStatus::create([
            "id" => 2,
            "name" => "Failed"
        ]);
        DataImportStatus::create([
            "id" => 3,
            "name" => "Completed"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('data_import_statuses');
    }
}
