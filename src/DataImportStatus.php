<?php

namespace Sirs\DataImporter;

use Illuminate\Database\Eloquent\Model;

class DataImportStatus extends Model
{
    protected $fillable = [
	    'name'
    ];

	public function dataImports()
	{
		return $this->hasMany('Sirs\DataImporter\DataImport');
	}
}
