<?php

namespace Sirs\DataImporter;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Sirs\DataImporter\Exceptions\DataImportException;
use Sirs\DataImporter\Exceptions\DataImportInvalidRowException;
use Sirs\DataImporter\Interfaces\FileProcessor;

/**
 * Imports a csv file
 *
 * @package default
 * @author
 **/
class CsvProcessor implements FileProcessor
{
    protected $compositeKey;
    protected $errors = [];
    protected $delimiter = ',';

    /**
     * Constructor
     *
     * @return void
     * @author
     **/
    public function __construct()
    {
    }

    public function processFile(DataImport $dataImport)
    {
        $filePath = config('data_importer.useRelativePaths') ? base_path($dataImport->file_path) : $dataImport->file_path;
        // Just make sure it's set since we're passing folderPath and fileName as arguments
        $rowProcessorClass = config('data_importer.useRelativePaths') ? config('data_importer.filesToRowProcessors.'. dirname($dataImport->file_path)) : config('data_importer.filesToRowProcessors.'.dirname($filePath));
        $rowProcessor = new $rowProcessorClass();

        $this->compositeKey = $rowProcessor->getCSVKey();

        // Start transaction

        DB::beginTransaction();

        try {
            if (!file_exists($filePath) || !is_readable($filePath)) {
                throw new DataImportException('File does not exist or is not readable at: '.$filePath);
            }

            $header = null; // var used to store header

            if (($handle = fopen($filePath, 'r')) !== false) {
                $i = 0;
                while (($row = fgetcsv($handle, 0, $this->delimiter)) !== false) {
                    $i++;
                    if (!$header) { // set the header row
                        $header = $row;
                    } else {
                        try {
                            $csvRow = array_combine($header, $row);
                        } catch (\Exception $e) {
                            throw new DataImportException('Number of columns in header row does not match number of columns in data row. Header Count = '.count($header).' Row Count='.count($row).' Problem Row#:'.$i);
                        }

                        $csvRowKey = $this->getRowKey($csvRow, $dataImport);

                        try {
                            $instance = $rowProcessor->processRow($csvRow, $dataImport);
                        } catch (DataImportInvalidRowException $e) {
                            if (config('app.debug', false)) {
                                throw $e;
                            }
                            $this->errors[] = [
                                implode('_', $this->compositeKey) => $csvRowKey,
                                'error' => $e->getMessage()
                            ];
                        } catch (Exception $e) {
                            if (config('app.debug', false)) {
                                throw $e;
                            }
                            $this->errors[] = [
                                implode('_', $this->compositeKey) => $csvRowKey,
                                'error' => [$e->getMessage()]
                            ];
                        }
                    }
                }

                // throw exception if all we have is 1 row
                if (!isset($csvRow)) {
                    throw new DataImportException('Unable to parse rows in this CSV file');
                }
                fclose($handle);
            }
        } catch (DataImportException $e) {
            if (config('app.debug', false)) {
                throw $e;
            }
            $this->errors[] = ['error' => $e->getMessage()];
        }

        // Check exception array
        // If not empty, kill transaction and return exception array
        if (!empty($this->errors)) {
            DB::rollBack();

            $dataImport->update([
                'json_errors' => json_encode($this->errors),
                'data_import_status_id' => 2
            ]);

            $errors = Collection::make($this->errors);

            // TODO: allow user to define what happens on errors.
            // Fire event?
            Mail::send('dataimporter::emails.data_import_failed_notification', ['data_import' => $dataImport, 'errors'=>$errors], function ($message) {
                $message->to(config('app.primary_programmer.email'), config('app.primary_programmer.name'))
                    ->subject(config('app.project_name').' - data import failed')
                    ->from('sirsteam@listserv.unc.edu', config('app.project_name'));
            });

            return $errors;
        }
        DB::commit();

        $dataImport->update([
                'data_import_status_id' => 3
            ]);

        // TODO: allow user to define actions after save
        // Fire Event?
        Mail::send('dataimporter::emails.data_import_success_notification', ['data_import' => $dataImport], function ($message) {
            $message->to(config('app.primary_programmer.email'), config('app.primary_programmer.name'))
                    ->subject(config('app.project_name').' - Data import success')
                    ->from('sirsteam@listserv.unc.edu', config('app.project_name'));
        });

        return true;
    }

    /**
     * Gets the $csvRow's composite key
     * @param  array $csvRow
     * @return string Key
     */
    public function getRowKey($csvRow)
    {
        $rowKey = [];
        foreach ($this->compositeKey as $key) {
            $rowKey[] = $csvRow[$key];
        }

        return implode('_', $rowKey);
    }

    public function setDelimiter($delimiter)
    {
        $this->delimiter = $delimiter;
    }
} // END class CsvImporter
