<?php
namespace Sirs\DataImporter\Traits;
trait DataImporterTraits {

	/**
	 * Display a listing of the channels associated with this recipient
	 *
	 * @return array of DataImport objects
	 */
	public function dataImport(){
   		return $this->belongsTo('Sirs\DataImporter\DataImport');
  	}

  	/**
	 * Display the fully qualified classname of this model
	 *
	 * @return string
	 */
  	public function classname(){
  		return __CLASS__;
  	}

}
?>