<p>An attempt to import data failed on: {{$data_import->updated_at}}. </p>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <p>The import for {{ $data_import->file_path }} failed with the following errors:</p>
        <ul>
            @foreach ($errors->all() as $error)
                <li>
                    @if ( count($error) > 1 )
                        <ul>
                            @foreach ($error as $e)
                                <li>{{ json_encode($e) }}</li>
                            @endforeach
                        </ul>
                    @else
                        {{ json_encode($error) }}
                    @endif
                </li>
            @endforeach
        </ul>
@endif